// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyBOuBy9_tLorU5Q6T3FnvZrKfeL9jlyupg',
    authDomain: 'appbolos.firebaseapp.com',
    databaseURL: 'https://appbolos.firebaseio.com',
    projectId: 'appbolos',
    storageBucket: 'appbolos.appspot.com',
    messagingSenderId: '1008485495206',
    appId: '1:1008485495206:web:6a786e47912469bfc39f67',
    measurementId: 'G-8D418K0NKQ'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
