import { Bolo } from './../models/bolo';
import { BolosService } from './../services/bolos.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TabsPage } from '../tabs/tabs.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  public bolos: Array<Bolo> = [];

  constructor(
    private bolosService: BolosService,
    private tabs: TabsPage
  ) {}

  ngOnInit() {
    this.getBolos().subscribe((data: any) => {
     this.bolos = data.bolos;
    });
  }

  getBolos(): Observable<Array<Bolo>>{
    return this.bolosService.getBolos();
  }

}
