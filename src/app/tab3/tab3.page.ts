import { carrinho } from 'src/app/services/carrinho';
import { ActivatedRoute, Router } from '@angular/router';
import { Bolo } from '../models/bolo';
import { BolosService } from '../services/bolos.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  id: number;
  produtos: any[] = [];
  pedido: any;
  bolos: Bolo[] = [];
  totalValor = 0;
  totalItens = 0;
  loading: any;
  lista: any[] = [];



  constructor(
    private bolosService: BolosService,
    private activatedRoute: ActivatedRoute,
    public afs: AngularFirestore,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private navCtrl: NavController,
    private router: Router
    ) {
    this.id = this.activatedRoute.snapshot.params.id;
  }


  ngOnInit() {
  }

  ionViewWillEnter() {
    this.produtos = [];
    this.presentLoading();
    this.afs.collection('pedidos')
    .get().subscribe( (querySnapshot) => {
        querySnapshot.forEach((doc) => {
           this.produtos.push(doc.data());
        });
        this.loading.dismiss();
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

}

