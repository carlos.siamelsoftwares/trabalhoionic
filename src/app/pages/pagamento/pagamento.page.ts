import { Pedidos } from './../../models/pedidos';
import { Cliente } from './../../models/cliente';
import { BolosService } from './../../services/bolos.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bolo } from 'src/app/models/bolo';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-pagamento',
  templateUrl: './pagamento.page.html',
  styleUrls: ['./pagamento.page.scss'],
})
export class PagamentoPage implements OnInit {

  qtde: number;
  bolo: Bolo = {};
  loading: any;
  id: number;
  cliente: Cliente = {};
  tipo: any;
  produtos: any[] = [];
  totalValor = 0;
  totalItens = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private bolosService: BolosService,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private navCtrl: NavController,
    public afs: AngularFirestore
  ) {
    this.qtde = this.activatedRoute.snapshot.params.qtde;
    this.id = this.activatedRoute.snapshot.params.id;
    this.tipo = this.activatedRoute.snapshot.params.tipo;
  }

  ngOnInit() {
    this.getBoloId();
    this.produtos = JSON.parse(localStorage.getItem('carrinho')) ? JSON.parse(localStorage.getItem('carrinho')).produto : undefined;
    if (this.produtos) {
      this.produtos.filter((data: any) => {
        this.totalValor += (data.bolo.preco * data.qtde);
        this.totalItens += data.qtde;
        console.log(this.totalItens);
      });
    }
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  getBoloId() {
    this.bolosService.getBolos().subscribe(data => {
      data.bolos.filter((d: Bolo) => {
        if (this.id == d.id) {
          this.bolo = d;
        }
      });
    });
  }

  async finalizarPedido() {
    await this.presentLoading();
    if (this.tipo === undefined) {
      this.afs.collection('pedidos').add({ produtos: [ { bolo: this.bolo, qtde: this.qtde }], cliente: this.cliente })
        .then(() => {
          this.navCtrl.navigateRoot('tabs/tab1').then(() => {
            this.loading.dismiss();
            this.presentToast('Pedido finalizado com sucesso!');
          });
        })
        .catch((error) => {
          console.error('Error writing document: ', error);
        });
    } else {
      this.afs.collection('pedidos').add({ produtos: this.produtos, cliente: this.cliente }).then(() => {
        localStorage.removeItem('carrinho');
        this.navCtrl.navigateRoot('tabs/tab2').then(() => {
          this.navCtrl.navigateRoot('tabs/tab1').then(() => {
            this.loading.dismiss();
            this.presentToast('Pedido finalizado com sucesso!');
          });
        });
      })
        .catch((error) => {
          console.error('Error writing document: ', error);
        });
    }
  }

}
