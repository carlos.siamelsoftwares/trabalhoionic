import { carrinho } from './../../services/carrinho';
import { BolosService } from './../../services/bolos.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Bolo } from 'src/app/models/bolo';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  id: number;
  loading: any;
  bolo: Bolo = {};
  qtdeItemCarrino = 0;
  qtdeItemPedido = 0;



  constructor(
    private loadingController: LoadingController,
    private toastController: ToastController,
    private bolosService: BolosService,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit() {
    if (this.id) { this.getBoloId(); }
  }

  getBoloId() {
    this.bolosService.getBolos().subscribe(data => {
      this.bolo = data.bolos[this.id - 1];
    });
  }

  async saveCarrinho(bolo: Bolo) {
    await this.presentLoading();

    if (this.id) {
      try {
        this.bolosService.addCarrinho(bolo, this.qtdeItemPedido);
        this.navCtrl.navigateRoot('tabs/tab1').then( () => {
          this.presentToast(this.qtdeItemPedido + ' bolos foram adicinado ao carrinho');
          this.loading.dismiss();
        }) ;
      } catch (error) {
        this.presentToast('Erro ao adicionar bolo ao carrinho! ' + error + this.id);
        this.loading.dismiss();
      }
    }
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  proximoPasso() {
    this.router.navigate(['/pagamento', {qtde: this.qtdeItemPedido, id: this.id}]);
  }

}
