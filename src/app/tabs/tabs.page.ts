import { Pedidos } from './../models/pedidos';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  qtdeCarrinho: number;

  constructor() {
  }

  ionViewWillEnter() {
    const pedido: Pedidos = JSON.parse(localStorage.getItem('carrinho'));
    this.qtdeCarrinho = pedido != undefined ? pedido.produto.length : 0;
  }


}
