import { carrinho } from 'src/app/services/carrinho';
import { ActivatedRoute, Router } from '@angular/router';
import { Bolo } from './../models/bolo';
import { BolosService } from './../services/bolos.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  id: number;
  produtos: [] = [];
  pedido: any;
  bolos: Bolo[] = [];
  totalValor = 0;
  totalItens = 0;
  loading: any;



  constructor(
    private bolosService: BolosService,
    private activatedRoute: ActivatedRoute,
    public afs: AngularFirestore,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private navCtrl: NavController,
    private router: Router
    ) {
    this.id = this.activatedRoute.snapshot.params.id;
  }


  ngOnInit() {
  }

  ionViewWillEnter() {
    this.totalValor = 0;
    this.totalItens = 0;
    this.pedido = JSON.parse(localStorage.getItem('carrinho'));
    if (this.pedido) {
      this.produtos = this.pedido.produto;

      this.produtos.filter((data: any) => {
        this.totalValor += (data.bolo.preco * data.qtde);
        this.totalItens += data.qtde;
      });
    }
  }

  proximoPasso() {
    this.navCtrl.navigateRoot(['/pagamento', {tipo: 'carrinho'}]);
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Por favor, aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

}

