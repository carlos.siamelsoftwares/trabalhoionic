import { Pedidos } from './../models/pedidos';
import { carrinho } from 'src/app/services/carrinho';
import { Bolo } from './../models/bolo';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { database } from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class BolosService {

  bolos: Array<Bolo> = [];

  constructor(
    private http: HttpClient,
    public afs: AngularFirestore
  ) { }

  addCarrinho(bolo: Bolo, qtde: number) {
    if (!carrinho.produto) {
      carrinho.produto = [{bolo, qtde}];
      localStorage.setItem('carrinho', JSON.stringify(carrinho));
    } else {
      let i = 0;
      let cont = 0;
      for ( i = 0; i < carrinho.produto.length; i ++ ) {
        if (carrinho.produto[i].bolo.id == bolo.id) {
          carrinho.produto[i].qtde = qtde;
          cont ++;
          break;
        }
      }
      if (cont === 0) {
        carrinho.produto.push({bolo, qtde});
      }
    }
    localStorage.setItem('carrinho', JSON.stringify(carrinho));
  }

  getBoloId(id: number) {
    this.getBolos().subscribe(data => {
      data.bolos.filter(( data: Bolo) => {
        if (id === data.id) {
          return data;
        }
      });
    });
  }




  getBolos(): Observable<any> {
    // this.afs.collection('itens').valueChanges().subscribe(data => {
    //   console.log(data);
    // });

    return this.http.get('../../assets/bolos.json');
  }

}
