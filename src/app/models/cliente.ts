export interface Cliente {
    nome?: string;
    telefone?: string;
    cep?: string;
}
