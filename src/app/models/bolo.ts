export interface Bolo {
    id?: number;
    nome?: string;
    descricao?: string;
    preco?: string;
    imagem?: string;
}
