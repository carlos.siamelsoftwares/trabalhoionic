import { Bolo } from './bolo';

export interface Pedidos {
    produto?: Array<{ bolo: Bolo, qtde?: number }>;
}
